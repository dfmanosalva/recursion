import java.util.Scanner;

public class Recursion1 
{
	public Recursion1(int pNumero)
	{
		
	}

	public void convertirABinario(int pNumero)
	{
		int bin = pNumero/2;
		int mod = pNumero%2;
		if(bin < 1)
		{
			System.out.print(mod);
		}
		else
		{
			convertirABinario(mod, bin);
			System.out.print(mod);
		}
		System.out.print("\n");
	}

	public void convertirABinario(int modulo, int pNumero)
	{
		int bin = pNumero/2;
		int mod = pNumero%2;
		if(bin < 1)
		{
			System.out.print(mod);
		}
		else
		{
			convertirABinario(mod, bin);
			System.out.print(mod);
		}
	}

	public static void main(String[] args) 
	{
		Scanner in = new Scanner(System.in);
		boolean fin = false;
		while(!fin)
		{
			System.out.println("Ingrese un numero");
			int numero = in.nextInt();
			if(numero == -1)
			{
				fin = true;
			}
			Recursion1 rec = new Recursion1(numero);
			System.out.print("numero binario --> ");
			rec.convertirABinario(numero);
			
		}
		in.close();
	}

}


public class Recursion 
{
	public int[] lista;

	public Recursion()
	{
		lista = new int[10];	
		for (int i = 0; i < lista.length; i++) 
		{
			lista[i] = (int) (Math.random()*100);
		}
		
		System.out.println("lista: ");
		for (int i = 0; i < lista.length; i++) 
		{
			System.out.print(lista[i]+",");
		}
		System.out.print("\n");
	}

	public int darMayor(int[] pLista)
	{
		return darMayor(pLista, 0 , lista.length-1);
	}

	public int darMayor(int[] listaparam, int lo , int hi )
	{
		if (lo >= hi)
		{
			if(listaparam[lo]>listaparam[hi])
			{
				return listaparam[lo];
			}
			else
				return listaparam[hi];
		}
		else
		{
			int mid = lo + (hi - lo)/2;
			int mayor1 = darMayor(listaparam, lo, mid);
			int mayor2 = darMayor(listaparam, mid+1, hi);
			if(mayor1 >mayor2)
			{
				return mayor1;
			}
			else
				return mayor2;
		}
	}

	public static void main(String[] args)
	{
		Recursion rec = new Recursion();
		System.out.println("mayor : " + rec.darMayor(rec.lista));
	}
}
